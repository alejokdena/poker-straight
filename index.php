<?php
//use this array for test
$cards = array(13, 2, 3, 4, 5, 11, 12);
if (sizeof($cards) < 5 || sizeof($cards) > 7) {
    echo "the size the cards must be between 5 and 7";
} elseif (max($cards) > 14) {
    echo max($cards)." this value is not allowed";
} else {
    echo "Input cards: ".implode(',', $cards)."\n";
    $cards = array_unique($cards);
    if (max($cards) == '14') {
        array_push($cards, 1);
    }
    rsort($cards);
    $set = array(array_shift($cards)); // start with the first card
    foreach ($cards as $card) {
        $lastCard = $set[count($set)-1];
        if ($lastCard - 1 != $card) {
            // not a chain anymore, "restart" from here
            $set = array($card);
        } else {
            $set[] = $card;
        }
        if (count($set) == 5) {
            break;
        }
    }
    if (count($set) == 5) {
        echo "Found a straight with ".implode(',', $set)."\n";
    } else {
        echo "No straight\n";
    }
}
